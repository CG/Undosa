# Undosa

![plateform: Linux | Windows | macOS](https://img.shields.io/badge/platform-Linux%20%7C%20Windows%20%7C%20macOS-lightgrey)
![python: 3.8](https://img.shields.io/badge/python-3.8-blue)
![taichi: v0.7.15](https://img.shields.io/badge/taichi-v0.7.15-blue)
![docs: LaTeX](https://img.shields.io/badge/docs-LaTeX-green)
![Code style: black](https://img.shields.io/badge/code%20style-black-000000)
![coverage: 0%](https://img.shields.io/badge/coverage-0%25-critical)

Undosa is a high performance cross-platform simulator for physical phenomena based on the [Taichi](https://github.com/taichi-dev/taichi) programming language.

## Setup

Please note that **only 64-bit Python 3.8 is supported** at the moment (e.g. ARM, 32-bit architecture, Python 3.9 are not supported).

If you are on Linux, please also install the ncurses library:

```
sudo yum install ncurses-compat-libs  # Fedora and RHEL derivatives
sudo apt install libtinfo5            # Debian derivatives
```

This application has been tested on Fedora 33 with Python 3.8.9, Fedora 34 with Python 3.8.8, and Windows 10 with Python 3.8.9.

An example setup using virtual environment is outlined below:

- Bash (Linux & macOS)

  ```
  python --version                  # make sure you are using python 3.8
  python -m venv .env
  source .env/bin/activate
  pip install -r requirements.txt
  ```

- PowerShell (Windows)

  ```
  python.exe --version              # make sure you are using python 3.8
  python.exe -m venv .env
  .\.env\Scripts\activate
  pip install -r requirements.txt
  ```

## Simulate scenes

To run the scene `squashed.json`:

```
python undosa/main.py scenes/squashed.json
```

To run the scene `snow-drop.json` with the same parameters as in the demo:

```
python undosa/main.py -p 20000 -g 128 -t 2e-4 scenes/snow-drop.json
```

### Usage

```
usage: main.py [-h] [-p particles] [-g grids] [-t dt] [-q {low,high,ultra}]
               [-r RESOLUTION] [--ply] [--img] [--tina]
               scene

Run the fluid simulator.

positional arguments:
  scene                 path to your JSON scene

optional arguments:
  -h, --help            show this help message and exit
  -p particles          number of particles, more is better
  -g grids              number of grids, more is better
  -t dt                 dt, less is better
  -q {low,high,ultra}, --quality {low,high,ultra}
                        preset for p, g, t
  -r RESOLUTION, --resolution RESOLUTION
                        gui resolution, also affects image output
  --ply                 enable ply output
  --img                 enable img output
  --tina                enable rendering with tina (experimental, CUDA only)
```

## Example

<a href="https://gitlab.cs.mcgill.ca/CG/Undosa/blob/master/scenes/snow-drop.json">
  <img src="https://gitlab.cs.mcgill.ca/CG/Undosa/raw/master/assets/snow-drop.gif">
</a>
