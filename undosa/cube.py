import taichi as ti
from typing import Tuple
from material import Material

Range = Tuple[float, float]


@ti.data_oriented
class Cube:
    def __init__(self, x: Range, y: Range, z: Range, prop: int, mat: Material):
        """Representation of a cube in our simulation

        Args:
            x (Range): from 0 to 1, the starting and ending x coordinate
            y (Range): from 0 to 1, the starting and ending y coordinate
            z (Range): from 0 to 1, the starting and ending z coordinate
            prop (int): unitless value that dictates the amount of particles
                this cube should take
            mat (Material): the material that will be used to fill this cube

        Attributes:
            x (Range): from 0 to 1, the starting and ending x coordinate
            y (Range): from 0 to 1, the starting and ending y coordinate
            z (Range): from 0 to 1, the starting and ending z coordinate
            proportion (int): unitless value that dictates the amount of
                particles this cube should take
            material (Material): the material that will be used to fill this
                cube

        """
        self.x = x
        self.y = y
        self.z = z
        self.proportion = prop
        self.material = mat

    @property
    def volume(self):
        return (
            self.proportion
            * (self.x[1] - self.x[0])
            * (self.y[1] - self.y[0])
            * (self.z[1] - self.z[0])
        )
