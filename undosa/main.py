#!/bin/python3
import argparse
import json
from pathlib import Path

import taichi as ti
from cube import Cube
from sandbox import Sandbox, T
from material import Material, MATERIAL_COLOR
import tina

parser = argparse.ArgumentParser(description="Run the fluid simulator.")
parser.add_argument("scene", help="path to your JSON scene")
parser.add_argument(
    "-p", metavar="particles", type=int, help="number of particles, more is better"
)
parser.add_argument(
    "-g", metavar="grids", type=int, help="number of grids, more is better"
)
parser.add_argument("-t", metavar="dt", type=float, help="dt, less is better")
parser.add_argument(
    "-q",
    "--quality",
    choices=["low", "high", "ultra"],
    default="low",
    help="preset for p, g, t",
)
parser.add_argument(
    "-r",
    "--resolution",
    type=int,
    default=512,
    help="gui resolution, also affects image output",
)
parser.add_argument(
    "--ply", help="enable ply output", default=False, action="store_true"
)
parser.add_argument(
    "--img", help="enable img output", default=False, action="store_true"
)
parser.add_argument(
    "--tina",
    help="enable rendering with tina (experimental, CUDA only)",
    default=False,
    action="store_true",
)

if __name__ == "__main__":
    args = parser.parse_args()
    cubes = []
    with open(args.scene) as file:
        data = json.load(file)
        for c in data["cubes"]:
            cubes.append(Cube(c["x"], c["y"], c["z"], c["prop"], Material[c["mat"]]))

    # parameters parsing
    ng = 32
    dt = 4e-4
    np = 8000

    if args.quality == "high":
        g = 64
        dt = 2e-4
        np = 20000
    elif args.quality == "ultra":
        np = 100000
        g = 128
        dt = 8e-5

    if args.g is not None:
        ng = args.g
    if args.t is not None:
        dt = args.t
    if args.p is not None:
        np = args.p

    if args.img or args.ply:
        Path(f"output/{Path(args.scene).stem}").mkdir(parents=True, exist_ok=True)
        if args.img:
            Path(f"output/{Path(args.scene).stem}/img").mkdir(parents=True)
        if args.ply:
            Path(f"output/{Path(args.scene).stem}/ply").mkdir(parents=True)

    frame_count = 0
    if args.tina:
        mciso = tina.MCISO((ng, ng, ng))
        scene = tina.Scene(smoothing=True, maxfaces=2 ** 18, ibl=True, ssao=True)
        material = tina.PBR(metallic=0.10, roughness=0.0)
        scene.add_object(mciso, material)
        sandbox = Sandbox(np, ng, dt)
        sandbox.init(cubes)
        gui = ti.GUI(
            "MLS-MPM 3D with materials", res=scene.res, background_color=0xFFFFFF
        )
        scene.init_control(gui, center=[0.5, 0.5, 0.5], radius=1.5)
    else:
        sandbox = Sandbox(np, ng, dt)
        sandbox.init(cubes)
        gui = ti.GUI(
            "MLS-MPM 3D with materials", res=args.resolution, background_color=0x1B2021
        )

    while gui.running and not gui.get_event(gui.ESCAPE):
        for s in range(int(2e-3 // sandbox.dt)):
            sandbox.substep()
        pos = sandbox.x.to_numpy()

        if args.ply:
            for cube in sandbox.cubes:
                writer = ti.PLYWriter(num_vertices=cube.end_idx - cube.start_idx)

                writer.add_vertex_pos(
                    pos[cube.start_idx : cube.end_idx, 0],
                    pos[cube.start_idx : cube.end_idx, 1],
                    pos[cube.start_idx : cube.end_idx, 2],
                )
                writer.export_frame(
                    gui.frame,
                    f"output/{Path(args.scene).stem}/ply/{cube.material}_frame_{cube.start_idx}.ply",
                )

        if args.tina:
            mciso.march(sandbox.x, w0=2, rad=0, sig=0)
            scene.render()
            gui.set_image(scene.img)
        else:
            radius = 1.5 if args.resolution < 900 else 2.0
            gui.circles(
                T(pos), radius=radius, color=MATERIAL_COLOR[sandbox.material.to_numpy()]
            )

        if args.img:
            gui.show(
                f"output/{Path(args.scene).stem}/img/frame_{str(frame_count).zfill(4)}.png"
            )
        else:
            gui.show()
        frame_count += 1
