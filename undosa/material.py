import taichi as ti
import numpy as np
from enum import IntEnum

"""Material constants"""


class Material(IntEnum):
    SAND = 3
    LIQUID = 2
    JELLY = 1
    SNOW = 0


MATERIAL_COLOR = np.empty(len(Material), dtype=np.uint32)
MATERIAL_COLOR[Material.SAND] = 0xE7C496
MATERIAL_COLOR[Material.LIQUID] = 0x068587
MATERIAL_COLOR[Material.JELLY] = 0xED553B
MATERIAL_COLOR[Material.SNOW] = 0xEEEEF0
