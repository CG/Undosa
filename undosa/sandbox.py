import sys
import taichi as ti
import numpy as np
from material import Material
from cube import Cube
from typing import List
import math

ti.init(arch=ti.gpu)

# dimension
dim = 3

# Young's modulus and Poisson's ratio
E = 0.1e4
nu = 0.2

# Lame parameters
mu_0 = E / (2 * (1 + nu))
lambda_0 = E * nu / ((1 + nu) * (1 - 2 * nu))

gravity = 9.8
bound = 3


@ti.data_oriented
class Sandbox:
    def __init__(self, p: int, g: int, t: float):
        """Simulation sandbox

        Create a new Sandbox that uses the moving least squares material point
        method to simulate the fluids.

        Args:
            p (int): the number of particles
            g (int): the number of grids
            t (float): dt

        Attributes:
            n_particles (int): the number of particles
            n_grid (int): the number of grids
            dt (float): dt
            dx (float): dx
            inv_dx (float): 1/dx
            p_vol (float): volume of a particle
            p_rho (float): density of a particle
            p_mass (float): mass of a particle
            x: position
            v: velocity
            C: affine velocity field
            F: deformation gradient
            material: material id
            Jp: plastic deformation
            grid_v: grid node momentum/velocity
            grid_m: grid node mass
        """
        self.n_particles = p
        self.n_grid = g
        self.dt = t
        self.dx = 1 / g
        self.inv_dx = float(g)
        self.p_vol = (self.dx * 0.5) ** 2
        self.p_rho = 1.0
        self.p_mass = self.p_vol * self.p_rho
        self.x = ti.Vector.field(dim, dtype=float, shape=p)
        self.v = ti.Vector.field(dim, dtype=float, shape=p)
        self.C = ti.Matrix.field(dim, dim, dtype=float, shape=p)
        self.F = ti.Matrix.field(dim, dim, dtype=float, shape=p)
        self.material = ti.field(dtype=int, shape=p)
        self.Jp = ti.field(dtype=float, shape=p)
        self.grid_v = ti.Vector.field(dim, dtype=float, shape=(g,) * dim)
        self.grid_m = ti.field(dtype=float, shape=(g,) * dim)

    @ti.kernel
    def substep(self):
        """Do a substep using the moving least squares material point method"""
        (
            n_grid,
            dt,
            dx,
            inv_dx,
            p_vol,
            p_mass,
            x,
            v,
            C,
            F,
            material,
            Jp,
            grid_v,
            grid_m,
        ) = ti.static(
            self.n_grid,
            self.dt,
            self.dx,
            self.inv_dx,
            self.p_vol,
            self.p_mass,
            self.x,
            self.v,
            self.C,
            self.F,
            self.material,
            self.Jp,
            self.grid_v,
            self.grid_m,
        )

        (SAND, LIQUID, JELLY, SNOW) = ti.static(
            Material.SAND, Material.LIQUID, Material.JELLY, Material.SNOW
        )

        for I in ti.grouped(grid_m):
            grid_v[I] = ti.zero(grid_v[I])
            grid_m[I] = 0

        ti.block_dim(n_grid)
        for p in x:  # P2G
            Xp = x[p] * inv_dx
            base = int(Xp - 0.5)
            fx = Xp - base
            # Quadratic kernels  [http://mpm.graphics   Eqn. 123, with x=fx, fx-1,fx-2]
            w = [0.5 * (1.5 - fx) ** 2, 0.75 - (fx - 1) ** 2, 0.5 * (fx - 0.5) ** 2]

            stress = -4 * dt * E * p_vol * (Jp[p] - 1) * inv_dx * inv_dx
            affine = ti.Matrix.identity(float, 3) * stress + p_mass * C[p]

            if material[p] != LIQUID:
                # deformation gradient update
                F[p] = (ti.Matrix.identity(float, 3) + dt * C[p]) @ F[p]

                h = 0.3  # jelly is softer
                if material[p] == SNOW:
                    # Hardening coefficient: snow gets harder when compressed
                    h = ti.exp(10 * (1.0 - Jp[p]))
                elif material[p] == SAND:
                    h = 0.5

                mu = mu_0 * h
                la = lambda_0 * h
                if material[p] == SAND:
                    mu = 70

                U, sig, V = ti.svd(F[p])
                J = 1.0
                for d in ti.static(range(3)):
                    new_sig = sig[d, d]
                    if material[p] == SNOW or material[p] == SAND:
                        # Plasticity
                        new_sig = min(max(sig[d, d], 1 - 2.5e-2), 1 + 4.5e-3)
                    Jp[p] *= sig[d, d] / new_sig
                    sig[d, d] = new_sig
                    J *= new_sig

                if material[p] == SNOW or material[p] == SAND:
                    # Reconstruct elastic deformation gradient after plasticity
                    F[p] = U @ sig @ V.transpose()

                stress_l = 2 * mu * (F[p] - U @ V.transpose()) @ F[
                    p
                ].transpose() + ti.Matrix.identity(float, 3) * la * J * (J - 1)
                stress_l = (-dt * p_vol * 4 * inv_dx * inv_dx) * stress_l
                affine = stress_l + p_mass * C[p]

            for offset in ti.static(ti.grouped(ti.ndrange(3, 3, 3))):
                dpos = (offset - fx) * dx

                weight = w[offset[0]][0] * w[offset[1]][1] * w[offset[2]][2]

                grid_v[base + offset] += weight * (p_mass * v[p] + affine @ dpos)
                grid_m[base + offset] += weight * p_mass

        for I in ti.grouped(grid_m):
            if grid_m[I] > 0:
                grid_v[I] /= grid_m[I]  # momentum to velocity
            grid_v[I][1] -= dt * gravity
            cond = I < bound and grid_v[I] < 0 or I > n_grid - bound and grid_v[I] > 0
            grid_v[I] = 0 if cond else grid_v[I]

        ti.block_dim(n_grid)
        for p in x:  # G2P
            Xp = x[p] * inv_dx
            base = int(Xp - 0.5)
            fx = Xp - base
            w = [0.5 * (1.5 - fx) ** 2, 0.75 - (fx - 1.0) ** 2, 0.5 * (fx - 0.5) ** 2]
            new_v = ti.zero(v[p])
            new_C = ti.zero(C[p])
            for offset in ti.static(ti.grouped(ti.ndrange(3, 3, 3))):
                dpos = offset - fx

                weight = w[offset[0]][0] * w[offset[1]][1] * w[offset[2]][2]

                g_v = grid_v[base + offset]
                new_v += weight * g_v
                new_C += 4 * inv_dx * weight * g_v.outer_product(dpos)

            if material[p] == LIQUID:
                Jp[p] *= 1 + dt * new_C.trace()

            v[p] = new_v
            x[p] += dt * v[p]  # advection
            C[p] = new_C

    def init(self, cubes: List[Cube]):
        """Fill the sandbox with the cubes, this can be called repeatly

        Args:
            cubes (List[Cube]): the initial cubes
        """
        self.cubes = cubes

        total = 0.0
        for cube in cubes:
            total += cube.volume

        i = 0
        for cube in cubes:
            cube.start_idx = i
            max = math.floor(cube.volume / total * self.n_particles) + i
            while i < max:
                self._set_particle(
                    i,
                    cube.x[0],
                    cube.x[1],
                    cube.y[0],
                    cube.y[1],
                    cube.z[0],
                    cube.z[1],
                    cube.material,
                )
                i += 1
            cube.end_idx = i

        while i < self.n_particles:
            self._set_particle(
                i,
                cubes[-1].x[0],
                cubes[-1].x[1],
                cubes[-1].y[0],
                cubes[-1].y[1],
                cubes[-1].z[0],
                cubes[-1].z[1],
                cubes[-1].material,
            )
            i += 1

    @ti.kernel
    def _set_particle(
        self,
        i: ti.i32,
        x_start: ti.f32,
        x_end: ti.f32,
        y_start: ti.f32,
        y_end: ti.f32,
        z_start: ti.f32,
        z_end: ti.f32,
        mat: ti.i32,
    ):
        """This is a kernel function that should be called by init.

        This function initialize the particle attributes at index i

        Args:
            i (ti.i32): the index
            x_start (ti.f32): starting position of x from 0 to 1
            x_end (ti.f32): ending position of x from 0 to 1
            y_start (ti.f32): starting position of y from 0 to 1
            y_end (ti.f32): ending position of y from 0 to 1
            z_start (ti.f32): starting position of z from 0 to 1
            z_end (ti.f32): ending position of z from 0 to 1
            mat (ti.i32): the material id
        """
        x, v, F, Jp, material = ti.static(
            self.x, self.v, self.F, self.Jp, self.material
        )

        x[i] = [
            ti.random() * (x_end - x_start) + x_start,
            ti.random() * (y_end - y_start) + y_start,
            ti.random() * (z_end - z_start) + z_start,
        ]
        v[i] = ti.Matrix([0] * dim)
        F[i] = ti.Matrix.identity(float, dim)
        Jp[i] = 1
        material[i] = mat


def T(a):
    """Create 2D GUI coordinates based on 3D coordinates

    Args:
        a: numpy array representing 3D positions

    Returns:
        numpy array representing 2D positions on screen
    """
    if dim == 2:
        return a

    phi, theta = np.radians(28), np.radians(32)

    a = a - 0.5
    x, y, z = a[:, 0], a[:, 1], a[:, 2]
    c, s = np.cos(phi), np.sin(phi)
    C, S = np.cos(theta), np.sin(theta)
    x, z = x * c + z * s, z * c - x * s
    u, v = x, y * C + z * S
    return np.array([u, v]).swapaxes(0, 1) + 0.5
