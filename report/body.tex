\section{Introduction}

In this project, we introduce a high performance cross-platform simulator for physical phenomena based on the Taichi Programming Language \cite{Hu19}. We implemented moving least square material point method, and our mordularized design allows for easy customization and can run on both CPU and GPU environments.

\section{Eulerian View and Laguarian View}

In the laguarian view of the fluids, the sensor moves with the object and records the position and velocity of the corresponding objects. In the eulerian view of the fluids, the sensor is stationary. The sensors on your grid will record the object as it passes by and record its material velocity. A physical solver usually has two components, advection (moving the objects) and interaction (external forces, enforce volume, etc).
Eulerian grids are really good at interaction, since it's easy to discretize, and allows for efficient neighbor lookup, but it is usually very bad at advection (loss of energy, etc).
Lagrangian particles are good at advection, since it's more conservative, but particle interactions can become tricky: performing neighbor look up requires complex data structure. So the big question is, can we somehow smartly combine Laguarian particles and eulerian grids?

\section{Material Point Method}

\begin{figure}[!h]
  \includegraphics[width=179pt,keepaspectratio]{mpm}
  \caption{Material Point Method Overview \cite{Stomakhin13}}
\end{figure}

\noindent
A solution to the problem is to use Laguarian particles to store most of the information, and use Eulerian grids to perform grid operation. Material Point Method (MPM) is a hybrid lagrangian-eulerian simulation scheme, and the particle carries a lot more information than just velocity. MPM is first introduced to the Graphics industry with the film Frozen by Walt Disney \cite{Stomakhin13}.
MPM has been gaining traction because it allows automatic coupling of different materials (liquid, solids, granular materials), automatic (self-)collision handling, and is capable of simulating large deformations.

\section{Moving Least Squares MPM}
Moving Least Squares Material Point Method (MLS-MPM) shares many similarities with Affine Particle-In-Cell (APIC) \cite{Jiang15} and the traditional MPM. Comparing MLS-MPM with APIC, we first recall that in APIC we perform the following operations:
\begin{enumerate}
  \item Particle to Grid Transfer
        \subitem Grid Momentum
        \[
          (m\m{v})_{i}^{n+1} = \sum_{p}\left[m_p\m{C}_{p}^{n}\right]w_{ip}(\m{x}_i-\m{x}_{p}^{n})
        \]
        \subitem Grid Mass
        \[
          m_{i}^{n+1} = \sum_p m_p w_{ip}
        \]
  \item Grid Operations
        \subitem Grid Velocity
        \[
          \m{\hat{v}}_{i}^{n+1} = \frac{(m\m{v})_{i}^{n+1}}{m_{i}^{n+1}}
        \]
        \subitem Pressure Projection
        \[
          \m{v}^{n+1} = \textbf{Project}\boldsymbol{(\m{\hat{v}}_{i}^{n+1})}
        \]
  \item Grid to Particle Transfer
        \subitem Particle Velocity
        \[
          \m{v}_{p}^{n+1} = \sum_{i}w_{ip}\m{v}_{i}^{n+1}
        \]
        \subitem Particle Velocity Gradient
        \[
          \m{C}_{p}^{n+1} = \frac{4}{\Delta x^2}\sum_i w_{ip} \m{v}_{i}^{n+1} (\m{x}_i - \m{x}_{p}^{n})^{T}
        \]
        \subitem Particle Position
        \[
          \underbrace{\m{x}_{p}^{n+1} = \m{x}_{p}^{n} + \Delta t \m{v}_{p}^{n+1}}_{\text{basically a simpletic euler}}
        \]
\end{enumerate}

Now let's take a look at MLS-MPM, the changes are highlighted:

\begin{enumerate}
  \item Particle to Grid Transfer

        MLS-MPM method use APIC \cite{Jiang15} to transfer mass and momentum from the particles to the grid.
        \subitem \textbf{Deformation Update}
        \[
          \m{F}_{p}^{n+1} = (\m{I} + \Delta t \m{C}_{p}^{n})\m{F}_{p}^{n}
        \]
        \subitem \textbf{Grid Momentum}
        \[
          (m\m{v})_{i}^{n+1} = \sum_{p}\left[m_p\m{C}_{p}^{n} - \frac{4\Delta t}{\Delta x^2} \sum_p V_{p}^{0} \m{P} (\m{F}_{p}^{n+1})\right]w_{ip}(\m{x}_i-\m{x}_{p}^{n})
        \]
        \subitem Grid Mass
        \[
          m_{i}^{n+1} = \sum_p m_p w_{ip}
        \]
  \item Grid Operations
        \subitem Grid Velocity
        \[
          \m{\hat{v}}_{i}^{n+1} = \frac{(m\m{v})_{i}^{n+1}}{m_{i}^{n+1}}
        \]
        \subitem \textbf{Grid Boundary Condition}
        \[
          \m{v}^{n+1} = \textbf{BC}(\m{\hat{v}}_{i}^{n+1})
        \]
  \item Grid to Particle Transfer

        Use APIC to transfer velocities and affine coefficients from the grid to the particles.
        \subitem Particle Velocity
        \[\m{v}_{p}^{n+1} = \sum_{i}w_{ip}\m{v}_{i}^{n+1}\]
        \subitem Particle Velocity Gradient
        \[
          \m{C}_{p}^{n+1} = \frac{4}{\Delta x^2}\sum_i w_{ip} \m{v}_{i}^{n+1} (\m{x}_i - \m{x}_{p}^{n})^{T}
        \]
        \subitem Particle Position
        \[
          \underbrace{\m{x}_{p}^{n+1} = \m{x}_{p}^{n} + \Delta t \m{v}_{p}^{n+1}}_{\text{updated with their new velocities}}
        \]
\end{enumerate}

\subsection{Deformation Update}

Lets take a closer look at the deformation update. Note that the deformation update evolve because $\Delta v = \frac{\6 \m{v} ^n}{\6 \m{x}^n} \bigr\rvert_{\m{x} = \m{x}_{p}^{n}} \neq 0$, so in essence, the local velocity field is not constant, and the material keeps deforming. So we can evaluate new deformation gradient by $\m{F}_{p}^{n+1} = (\m{I} + \Delta t \m{\Delta v})\m{F}_{p}^{n}$.
Different from traditional MPM, the MLS-MPM deformation gradiant update directly reuse quantities from APIC and does not involve any evaluation of the interpolation function gradient throughout the algorithm, so therefore we have $\m{F}_{p}^{n+1} = (\m{I} + \Delta t \m{C_{p}^{n}})\m{F}_{p}^{n}$.

\subsection{Grid Momentum}
There are two momentum terms: $m_p\m{C}_{p}^{n}w_{ip}(\m{x}_i-\m{x}_{p}^{n})$ which is directly from APIC, and $\Delta t \m{f}_{ip} = -\frac{4\Delta t}{\Delta x^2} \sum_p V_{p}^{0} \m{P} (\m{F}_{p}^{n+1}) w_{ip}(\m{x}_i-\m{x}_{p}^{n})$ which is the particle elastic force (impulse). Now, where did we get this?
Assuming hyperelastic materials, Deriving $\m{f}_i$ using potential energy gradients:
\begin{align*}
  \underbrace{\vphantom{\psi_{p}}U}_{\mathclap{\substack{\vphantom{p}\text{total elastic} \\ \text{potential energy}}}} & = \sum \overbrace{V_{p}^{0}}^{\mathclap{\text{particle initial volume}}} \underbrace{\psi_{p}}_{\mathclap{\substack{\text{elastic energy} \\ \text{density of particle}}}} \m{F}_{p} \\
  \m{f}_i & =  - \frac{\6 U}{\6 \m{x}_i}
\end{align*}
Assume we move forward $\tau \rightarrow 0$ and then compute deformed grid node location $\m{\hat{x}}_i = \m{x}_i + \tau{\m{v}_i}$, $\m{C}_p = \frac{4}{\Delta x^2} \sum_i w_{ip} \m{v}_i (\m{x}_i - \m{x}_p)^{T}$, updated $\m{F}^{'}_{p} = (\m{I} + \tau \m{C}_p) \m{F}_p$:
\begin{align*}
  \m{f}_i & = - \frac{\6 U}{\6 \m{x}_i} = - \sum V_{p}^{0} \frac{\6 \psi (\m{F}^{'}_p)}{\6 \m{\hat{x}}_i}                                                   \\
          & = - \sum \frac{V_{p}^{0}}{\tau} \frac{\6 \psi (\m{F}^{'}_p)}{\6 \m{v}_i}                                                                        \\
          & = - \sum \frac{V_{p}^{0}}{\tau} \frac{\6 \psi (\m{F}^{'}_p)}{\6\m{F}^{'}_p} \frac{\6\m{F}^{'}_p}{\6\m{C}_p} \frac {\6\m{C}_p} {\6\m{v}_{i}^{n}} \\
          & = - \sum \frac{V_{p}^{0}}{\tau} \m{P}_p(\m{F}^{'}_{p}) \tau \m{F}_{p}^{T} \frac{4w_{ip}}{\Delta{x^2}}(\m{x}_i - \m{x}_p)
\end{align*}

\subsection{Enforcing Boundary Conditions}

Boundary conditions in MPM should be applied to the grid. For all grid nodes i within the boundary:

\begin{align*}
  \m{v}_{i}^{n+1} & = \text{BC}_{\text{sticky}} (\m{\hat{v}}_{i}^{n+1} ) = 0                                                                          \\
  \m{v}_{i}^{n+1} & = \text{BC}_{\text{slip}} (\m{\hat{v}}_{i}^{n+1} ) = \m{\hat{v}}_{i}^{n+1} - \m{n}(n^{T}\m{\hat{v}}_{i}^{n+1} )                   \\
  \m{v}_{i}^{n+1} & = \text{BC}_{\text{separate}} (\m{\hat{v}}_{i}^{n+1} ) = \m{\hat{v}}_{i}^{n+1} - \m{n} \cdot \min(n^{T}\m{\hat{v}}_{i}^{n+1}, 0 )
\end{align*}

\subsection{Constitutive Models for Elastic Solids}

Deformation update:
\[
  \m{F}_{p}^{n+1} = (\m{I} + \Delta t \m{C_{p}^{n}})\m{F}_{p}^{n}
\]
PK1 stresses of hyperelastic material models (fixed corotated):
\begin{align*}
  \m{\psi(F)} & = \mu \sum_i(\underbrace{\sigma_i}_{\mathclap{\text{singular values of } \m{F}}} - 1)^2 + \frac{\lambda}{2}(J-1)^2 \\
  \m{P(F)}    & = \frac{d\psi}{d\m{F}} = 2 \mu (\m{F} - \m{R}) + \lambda (J - 1) J\m{F}^{-T}
\end{align*}

\subsection{Simulating Weakly Compressable Fluids}

Because we are lazy, we want to make our solver as simple as possible. So we set $\mu$ to zero in fixed corotated model.

\subsection{Simulating Elastoplastic Solids}

In hyperelastic settings, the potential energy penalizes elastic deformation only.
\[\m{F}_p = \m{F}_{p,\text{elastic}} \m{F}_{p,\text{plastic}}, \m{\psi}_{p}^{n} = \psi(\m{F}_{p, \text{elastic}})\]
In our simulator we used the "box" yield criterion as seen in \cite{Stomakhin13}. Our deformation update:

\begin{enumerate}
  \item Evolve
        \[
          {\m{\hat{F}}_{p}^{n+1}} = (\m{I} + \Delta t \m{C}_{p}^{n}) \m{F}_{p,\text{elastic}}^{n}
        \]
  \item SVD
        \[
          \m{\hat{F}}_{p}^{n+1} = \m{U} \m{\hat{\Sigma}} \m{V}^{T}
        \]
  \item Clamping
        \[
          \m{\Sigma}_{ii} = \underbrace{\max(\min(\m{\hat{\Sigma}}_{ii}, 1 + \theta_s), 1- \theta_c)}_{\text{forget the larger deformation}}
        \]
  \item Reconstruct
        \[
          \m{F}_{p, \text{elastic}}^{n+1} = \m{U} \m{\Sigma} \m{V}^{T}
        \]
        Move clamped parts to
        \[ \m{F}_{p, \text{plastic}}^{n+1}\]
\end{enumerate}

\subsection{Moving Least Squares Discretization}

Besides the particles and grids, there is another important concept with MPM: that it is the continuous field approximated by discrete particles and nodes. Essentially, MPM is a discretization method that projects the continuous quantities to discrete particles and nodes.
In 2D and 3D, you can get least squares reconstructions using basis functions. In the case of APIC \cite{Jiang15}, it uses a linear basis. To illustrate how this works, see the following diagram.

\begin{figure}[!h]
  \includegraphics[width=135pt,keepaspectratio]{apic}
  \caption{Superimposing the constant and linear functions}
\end{figure}

\noindent
We start by picking one constant function that minimizes least squares reconstruction error (smaller distance means better conversation of energy). We then super-impose the constant and linear functions (slope of the points) to obtain a much better reconstruction. In the context of APIC, it stores information about the velocity field gradient in addition to the constant velocity field. There is actually another way to reconstruct the continuous function, which is spline interpolation, commonly used in traditional MPM.

\begin{figure}[!h]
  \includegraphics[width=135pt,keepaspectratio]{splineinterp1}
  \caption{“Shape functions” in FEM and MPM}
\end{figure}

\noindent
Instead of using polynomials, we can use small splines at each node position. This is essentially the “shape function” approach in FEM and MPM.
\begin{figure}[!h]
  \includegraphics[width=135pt,keepaspectratio]{splineinterp2.png}
  \caption{Super Imposed Shape Functions: Continuous Function from Discrete DoFs}
\end{figure}

\noindent
By adding these splines together, we get a smooth reconstructed function. This method is the classical way to discrete PDEs in weak-form based methods such as FEM and MPM.

\noindent
So we have talked about two ways of reconstructing a continuous field out of discrete samples. It is common practice to use MLS interpolation for
APIC, and B-Spline interpolation for MPM discretization. But can we use a single interpolation method for both? From \cite{Hu19} we know that when you use B-Spline with APIC, it leads to no angular momentum conservation. However, using MLS interpolation for MPM discretization worked surprisingly well.

\begin{table}[!h]

  \begin{tabular}{| c | c | c |}\hline
                                   & \thead{APIC}         & \thead{MPM \\ Discretization} \\  \hline
    \thead{Moving Least                                                \\ Squares Interpolation} & YES                              & MLS-MPM            \\  \hline
    \thead{B-Spline Interpolation} & \makecell{No Angular              \\ Momentum \\ Conservation} & YES                \\ \hline
  \end{tabular}
\end{table}

\section{Summary}
Comparing MLS-MPM with traditional MPM, we can see that there is some difference when we update grid momentum and when we carry out the F update while updating the particle deformation gradient using the MLS approximation to the velocity gradient. In fact, both modifications made it not only simpler to implement but also more performant: Directly reusing APIC $\m{C}_p$ as an approximation of $\Delta\m{v}$ for deformation gradient update means we do not need to evaluate $\Delta w_{ip}$. Additionally, it is easier to move deformation update from G2P to P2G, because we only need $\m{C}_p$ for deformation update. Finally, in the P2G step the APIC and MLS-MPM momentum contribution can be fused, since they are both "MLS".
